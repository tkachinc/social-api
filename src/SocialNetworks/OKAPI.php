<?php
namespace TkachInc\SocialApi\SocialNetworks;

use TkachInc\Core\Log\FastLog;
use TkachInc\Engine\Services\Request\CURL\CURLManager;
use TkachInc\Engine\Services\Request\Request;
use TkachInc\SocialApi\SocialApiException;
use TkachInc\SocialApi\SocialApiFactory;
use TkachInc\SocialApi\SocialConfigs\OKConfig;
use TkachInc\SocialApi\SocialRef;
use TkachInc\SocialApi\SocialUser;

/**
 * Class ApiCM
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class OKAPI extends AbstractAPI
{
	/**
	 * @var OKConfig
	 */
	protected $config;

	/**
	 * @var array
	 */
	protected $userFields = [
		'uid',
		'first_name',
		'last_name',
		'gender',
		'locale',
		'location',
		'birthday',
		'pic190x190',
	];

	/**
	 * OKAPI constructor.
	 *
	 * @param OKConfig $config
	 */
	public function __construct(OKConfig $config)
	{
		parent::__construct($config);
	}

	/**
	 * @param       $method
	 * @param array $params
	 * @param array $config
	 * @return mixed
	 * @throws SocialApiException
	 */
	public function sendRequest($method, Array $params = [], Array $config = [])
	{
		$params['application_key'] = $this->config->getAppId();
		$params['method'] = $method;
		$params['format'] = 'json';
		if (isset($config['useSessionKey']) && $config['useSessionKey'] === true) {
			$sessionSecretKey = $this->config->getSessionSecretKey();
			$sessionKey = $this->config->getSessionKey();
			if ($sessionKey !== null) {
				$params['session_key'] = $sessionKey;
			}
			if ($sessionSecretKey !== null) {
				$params['sig'] = $this->apiMakeSignature($params, $sessionSecretKey);
			} else {
				$params['sig'] = $this->apiMakeSignature($params, $this->config->getAppSecret());
			}
		} else {
			$params['sig'] = $this->apiMakeSignature($params, $this->config->getAppSecret());
		}

		$timeout = $this->config->get('timeout', 3);
		$curl = new CURLManager($this->config->getAPIURL());
		$reply = $curl->sendPost($params)->setTimeout($timeout)->exec()->getContent();
		$reply = json_decode($reply, true);

		if (!empty($reply['error_code'])) {
			//throw new SocialApiException($reply['error_msg']);
			FastLog::add(
				'ERROR',
				[
					'error_code' => $reply['error_code'],
				]
			);
		}

		return $reply;
	}

	/**
	 * @param array $params
	 * @param null $secret
	 * @return string
	 */
	private function apiMakeSignature(Array $params, $secret = null)
	{
		ksort($params);
		$sig = urldecode(http_build_query($params, '', ''));

		return md5($sig . $secret);
	}

	/**
	 * @return array
	 */
	public function decodeExternalData()
	{
		$hash = Request::getGetOrPost('custom_args');
		$vars = [];
		if ($hash != '') {
			$hash = rawurldecode($hash);
			parse_str($hash, $vars);
		}

		return $vars;
	}

	/**
	 * @param null $socId
	 * @param array $args
	 * @return mixed
	 */
	public function getRef($socId = null, Array $args = [])
	{
		$refId = null;
		$refPostId = null;
		$refUserId = null;

		if (isset($args['referer']) && !empty($args['referer'])) {
			$refSrc = $args['referer'];
		} else {
			$refSrc = Request::getGetOrPost('refplace', 'direct');
			switch ($refSrc) {
				case 'friend_invitation':
					$refId = null;
					$refUserId = Request::getGetOrPost('referer', null);
					break;
				case 'passive_friend_invitation':
					$refId = null;
					$refUserId = Request::getGetOrPost('referer', null);
					break;
				case 'friend_feed':
					$refId = null;
					$refUserId = Request::getGetOrPost('referer', null);
					break;
				default:
					$refId = Request::getGetOrPost('referer', null);
					$refUserId = null;
					break;
			}
		}

		return new SocialRef($refSrc, $refId, $refUserId, $refPostId);
	}

	/**
	 * @param array $config
	 * @return mixed
	 */
	public function getFriendAppList(Array $config = [])
	{
		$userInfo = $this->sendRequest('friends.getAppUsers', [], ['useSessionKey' => true]);
		if (isset($userInfo['uids'])) {
			return $userInfo['uids'];
		}

		return $userInfo;
	}

	/**
	 * @param array $config
	 * @return mixed
	 */
	public function getFriendList(Array $config = [])
	{
		if (!isset($config['socId'])) {
			return [];
		}
		$userInfo = $this->sendRequest('friends.get', ['uid' => $config['socId']], ['useSessionKey' => true]);

		return $userInfo;
	}

	/**
	 * @param array $queryParams
	 * @param array $hashParams
	 * @return mixed
	 */
	public function getAppURL(Array $queryParams = [], Array $hashParams = [])
	{
		return $this->prepareURL(
            $this->config->getAppURL() . $this->config->getAppId(),
			$queryParams,
			$hashParams
		);
	}

	/**
	 * @param null $socId
	 * @param array $customFields
	 * @param array $userInfo
	 * @return mixed
	 * @throws SocialApiException
	 */
	public function getUserInfo($socId = null, Array $customFields = [], Array $userInfo = [])
	{
		//try {
		if (!isset($socId)) {
			throw new SocialApiException('Not found socId');
		}

		if (empty($customFields)) {
			$request = implode(',', $this->userFields);
		} else {
			$request = implode(',', $customFields);
		}

		if (empty($userInfo)) {
			$userInfo = $this->sendRequest(
				'users.getInfo',
				[
					'uids'   => $socId,
					'fields' => $request,
				],
				['useSessionKey' => true]
			);
			if (empty($userInfo)) {
				throw new SocialApiException('Server return empty result');
			}
			$userInfo = end($userInfo);
		}

		if (empty($userInfo) || !isset($userInfo['uid'])) {
			throw new SocialApiException('Not found required params');
		}

		$city = isset($userInfo['location']['city']) ? $userInfo['location']['city'] : '';
		$country = isset($userInfo['location']['country']) ? $userInfo['location']['country'] : '';

		if (isset($userInfo['birthday'])) {
			$dataArray = explode('-', $userInfo['birthday']);
			if (count($dataArray) === 3) {
				$date = new \DateTime($userInfo['birthday']);
				$birthD = $date->format('d');
				$birthM = $date->format('m');
				$birthY = $date->format('Y');
			} else {
				$birthD = isset($dataArray[2]) ? $dataArray[2] : 0;
				$birthM = isset($dataArray[1]) ? $dataArray[1] : 0;
				$birthY = isset($dataArray[0]) ? $dataArray[0] : 0;
			}
		} else {
			$birthD = 0;
			$birthM = 0;
			$birthY = 0;
		}

		$user = new SocialUser(
			OKConfig::getSocialNetwork(),
			$userInfo['uid'],
			isset($userInfo['first_name']) ? $userInfo['first_name'] : '',
			isset($userInfo['last_name']) ? $userInfo['last_name'] : '',
			$city,
			$country,
			isset($userInfo['gender']) ? $userInfo['gender'] : '',
			isset($userInfo['locale']) ? $userInfo['locale'] : '',
			$birthM,
			$birthD,
			$birthY,
			isset($userInfo['timezone']) ? $userInfo['timezone'] : '',
			isset($userInfo['pic190x190']) ? $userInfo['pic190x190'] : '',
			isset($userInfo['email']) ? $userInfo['email'] : '',
			$userInfo
		);

		return $user;
		//} catch (\Exception $e) {
		//	FastLog::add('ERROR',
		//		[
		//			'message' => $e->getMessage(),
		//			'code'    => $e->getCode(),
		//			'file'    => $e->getFile(),
		//			'line'    => $e->getLine()
		//		]
		//	);
		//}
		//return new SocialUser($socId, '', '', '', '', '', '', 0, 0, 0, '', '', '', []);
	}

	/**
	 * @param $array
	 * @return bool|string
	 * @throws \Exception
	 */
	public function requestValidate(Array $array)
	{
		if (isset($array['logged_user_id'], $array['session_key'], $array['auth_sig'])) {
			if ($array['auth_sig'] == $this->getAuthSig($array['logged_user_id'], $array['session_key'])) {
				return $array['logged_user_id'];
			}

			return false;
		} else {
			$sig = isset($array['sig']) ? (string)$array['sig'] : '';
			unset($array['net_id'], $array['country'], $array['city'], $array['sig']);
			ksort($array);

			return ($sig == md5(
				urldecode(http_build_query($array, '', '')) . $this->config->getAppSecret()
			) ? true : false);
		}
	}

	/**
	 * @param $uid
	 * @param $session_key
	 * @return string
	 */
	private function getAuthSig($uid, $session_key)
	{
		return md5($uid . $session_key . $this->config->getAppSecret());
	}

	/**
	 * @param array $params
	 * @return bool|string
	 * @throws \Exception
	 */
	public function checkPayment(Array $params)
	{
		return $this->requestValidate($params);
	}
}