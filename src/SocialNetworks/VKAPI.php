<?php
namespace TkachInc\SocialApi\SocialNetworks;

use TkachInc\Core\Log\FastLog;
use TkachInc\Engine\Services\Request\CURL\CURLManager;
use TkachInc\Engine\Services\Request\Request;
use TkachInc\SocialApi\SocialApiException;
use TkachInc\SocialApi\SocialApiFactory;
use TkachInc\SocialApi\SocialConfigs\VKConfig;
use TkachInc\SocialApi\SocialRef;
use TkachInc\SocialApi\SocialUser;

/**
 * Class ApiVK
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class VKAPI extends AbstractAPI
{
	/**
	 * @var VKConfig
	 */
	protected $config;

	const API_VERSION = '5.52';
	const DEFAULT_LANG = 'ru';

	/**
	 * @var array
	 */
	protected $userFields = [
		'id',
		'first_name',
		'last_name',
		'sex',
		'email',
		'country',
		'city',
		'bdate',
		'timezone',
		'photo_200',
	];

	/**
	 * VKAPI constructor.
	 *
	 * @param VKConfig $config
	 */
	public function __construct(VKConfig $config)
	{
		parent::__construct($config);
	}

	/**
	 * @param       $method
	 * @param array ...$params
	 * @param array $config
	 * @return mixed|string
	 * @throws SocialApiException
	 */
	public function sendRequest($method, Array $params = [], Array $config = [])
	{
		$params += [
			'api_id'    => $this->config->getAppId(),
			'method'    => $method,
			'format'    => 'json',
			'https'     => 1,
			'random'    => rand(100000, 999999),
			'timestamp' => time(),
		];

		if ($this->isSecureMethod($method)) {
			$params['access_token'] = $this->config->getAppId();
			$params['client_secret'] = $this->config->getAppSecret();
		}
		if (isset($config['useSessionKey']) && $config['useSessionKey'] === true) {
			$params['access_token'] = $this->config->getAccessToken();
		}

		$lang = $this->config->get('lang', self::DEFAULT_LANG);
		if (!isset($params['lang']) && !empty($lang)) {
			$params['lang'] = $lang;
		}

		$params['sig'] = $this->apiMakeSignature($params);
		$params['v'] = self::API_VERSION;

		$url = $this->config->getAPIURL() . $method;

		$timeout = $this->config->get('timeout', 3);
		$curl = new CURLManager($url);
		$reply = $curl->sendPost($params)->setTimeout($timeout)->exec()->getContent();

		$reply = json_decode($reply, true);

		if (!empty($reply['error'])) {
			//throw new SocialApiException($reply['error']['error_msg']);
			FastLog::add('ERROR', ['error' => $reply['error']]);
		}

		return $reply;
	}

	/**
	 * @param $params
	 * @return mixed
	 */
	private function apiMakeSignature(Array $params)
	{
		ksort($params);

		return md5(
			urldecode(http_build_query($params, '', '')) . $this->config->getAppSecret()
		);
	}

	/**
	 * @param $method
	 * @return bool
	 */
	private function isSecureMethod($method)
	{
		$secureMethods = [
			'secure.getTransactionsHistory',
			'secure.getSMSHistory',
			'secure.sendSMSNotification',
			'secure.sendNotification',
			'secure.setCounter',
			'secure.setUserLevel',
			'secure.addAppEvent',
			'secure.checkToken',
		];

		return in_array($method, $secureMethods);
	}

	/**
	 * @return string
	 */
	public function getSocIdInIframe()
	{
		return (string)Request::getGetOrPost('viewer_id', '');
	}

	/**
	 * @return array
	 */
	public function decodeExternalData()
	{
		$requestKey = isset($_GET['request_key']) ? (string)$_GET['request_key'] : '';
		$hash = isset($_GET['hash']) ? (string)$_GET['hash'] : '';
		parse_str($requestKey, $requestKey);
		parse_str($hash, $hash);
		$appParams = array_merge($requestKey, $hash);

		return $appParams;
	}

	/**
	 * @param null $socId
	 * @param array $args
	 * @return mixed
	 */
	public function getRef($socId = null, Array $args = [])
	{
		$refId = null;
		$refPostId = null;
		$refUserId = null;
		if (isset($args['referer']) && !empty($args['referer'])) {
			$refSrc = $args['referer'];
		} else {
			$refUserId = Request::getGetOrPost('user_id', null);
			$refPostId = Request::getGetOrPost('post_id', null);
			$refSrc = Request::getGetOrPost('referrer', 'direct');
		}
		if ($refSrc === 'unknown') {
			$refSrc = 'direct';
		}

		return new SocialRef($refSrc, $refId, $refUserId, $refPostId);
	}

	/**
	 * @param array $config
	 * @return mixed
	 */
	public function getFriendAppList(Array $config = [])
	{

		$friends = [];
		$vkFriends = $this->sendRequest('friends.getAppUsers', [], ['useSessionKey' => true]);
		if (isset($vkFriends['response'])) {
			$friends = $vkFriends['response'];
		}

		return $friends;
	}

	/**
	 * @param array $config
	 * @return mixed
	 */
	public function getFriendList(Array $config = [])
	{

		$friends = [];
		$vkFriends = $this->sendRequest('friends.get');
		if (isset($vkFriends['response'])) {
			$friends = $vkFriends['response'];
		}

		return $friends;
	}

	/**
	 * @param array $queryParams
	 * @param array $hashParams
	 * @return mixed
	 */
	public function getAppURL(Array $queryParams = [], Array $hashParams = [])
	{
		return $this->prepareURL(
            $this->config->getAppURL() . $this->config->getAppId(),
			$queryParams,
			$hashParams
		);
	}

	/**
	 * @param null $socId
	 * @param array $customFields
	 * @param array $userInfo
	 * @return SocialUser
	 * @throws SocialApiException
	 */
	public function getUserInfo($socId = null, Array $customFields = [], Array $userInfo = [])
	{
		//try {
		if (!isset($socId)) {
			throw new SocialApiException('Not found socId');
		}

		if (empty($customFields)) {
			$request = implode(',', $this->userFields);
		} else {
			$request = implode(',', $customFields);
		}

		if (empty($userInfo) || (!isset($userInfo['id']) && !isset($userInfo['uid']))) {
			$userInfo = $this->sendRequest(
				'users.get',
				[
					'user_ids' => $socId,
					'fields'   => $request,
				]
			);
			$userInfo = isset($userInfo['response'][0]) ? $userInfo['response'][0] : [];
			if (empty($userInfo) || !isset($userInfo['id'])) {
				throw new SocialApiException('Not found required params');
			}
		}

		$city = isset($userInfo['city']['title']) ? trim($userInfo['city']['title']) : '';
		$country = isset($userInfo['country']['title']) ? trim($userInfo['country']['title']) : '';

		if (isset($userInfo['bdate'])) {
			$dataArray = explode('.', $userInfo['bdate']);
			if (count($dataArray) === 3) {
				$date = new \DateTime($userInfo['bdate']);
				$birthD = $date->format('d');
				$birthM = $date->format('m');
				$birthY = $date->format('Y');
			} else {
				$birthD = (int)isset($dataArray[0]) ? $dataArray[0] : 0;
				$birthM = (int)isset($dataArray[1]) ? $dataArray[1] : 0;
				$birthY = (int)isset($dataArray[2]) ? $dataArray[2] : 0;
			}
		} else {
			$birthD = 0;
			$birthM = 0;
			$birthY = 0;
		}

		$gender = isset($userInfo['sex']) ? ($userInfo['sex'] == 2) ? 'male' : 'female' : '';

		if (isset($userInfo['uid'])) {
			$userInfo['id'] = $userInfo['uid'];
		}
		$user = new SocialUser(
			VKConfig::getSocialNetwork(),
			$userInfo['id'],
			isset($userInfo['first_name']) ? $userInfo['first_name'] : '',
			isset($userInfo['last_name']) ? $userInfo['last_name'] : '',
			$city,
			$country,
			$gender,
			'',
			$birthM,
			$birthD,
			$birthY,
			isset($userInfo['timezone']) ? $userInfo['timezone'] : '',
			isset($userInfo['photo_200']) ? $userInfo['photo_200'] : '',
			isset($userInfo['email']) ? $userInfo['email'] : '',
			$userInfo
		);

		return $user;
		//} catch (\Exception $e) {
		//	FastLog::add(
		//		'ERROR',
		//		[
		//			'message' => $e->getMessage(),
		//			'code'    => $e->getCode(),
		//			'file'    => $e->getFile(),
		//			'line'    => $e->getLine()
		//		]
		//	);
		//}
		//
		//return new SocialUser($socId, '', '', '', '', '', '', 0, 0, 0, '', '', '', []);
	}

	/**
	 * @param $user_id
	 * @return string
	 */
	private function getAuthKey($user_id)
	{
		return md5($this->config->getAppId() . '_' . $user_id . '_' . $this->config->getAppSecret());
	}

	/**
	 * @param $array
	 * @return bool|string
	 */
	public function requestValidate(Array $array)
	{
		if (isset($array['viewer_id'], $array['auth_key']) &&
			$array['auth_key'] == $this->getAuthKey($array['viewer_id'])
		) {
			return $array['viewer_id'];
		} else {
			if (isset($array['sig'])) {
				$sig = $array['sig'];
				$request = $array;
				unset($request['sig']);
				ksort($request);
				$requestCheck = ($sig == md5(
					urldecode(http_build_query($request, '', '')) . $this->config->getAppSecret()
				) ? true : false);
			} else {
				$requestCheck = false;
			}
		}

		return $requestCheck;
	}

	/**
	 * @param array $params
	 * @return bool|string
	 * @throws \Exception
	 */
	public function checkPayment(Array $params)
	{
		return $this->requestValidate($params);
	}
}