<?php
namespace TkachInc\SocialApi\SocialNetworks;

use Facebook\Facebook;
use Facebook\GraphNodes\GraphPicture;
use Facebook\GraphNodes\GraphUser;
use TkachInc\Core\Log\FastLog;
use TkachInc\Engine\Services\Helpers\Base64URL;
use TkachInc\Engine\Services\Request\Request;
use TkachInc\SocialApi\SocialApiException;
use TkachInc\SocialApi\SocialConfigs\FBConfig;
use TkachInc\SocialApi\SocialRef;
use TkachInc\SocialApi\SocialUser;

/**
 * Class ApiFB
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class FBAPI extends AbstractAPI
{
	/**
	 * @var FBConfig
	 */
	protected $config;

	/**
	 * @var array
	 */
	protected $userFields = [
		'id',
		'first_name',
		'last_name',
		'gender',
		'email',
		'locale',
		'location',
		'birthday',
		'timezone',
		'picture.width(200).height(200)',
	];

	/**
	 * @var Facebook
	 */
	public $app;

	/**
	 * @var \Facebook\Authentication\AccessToken|null
	 */
	public $accessToken = null;

	/**
	 * @var mixed
	 */
	protected $appId;

	/**
	 * @var mixed
	 */
	protected $appShortName;

	/**
	 * @var mixed
	 */
	protected $appSecretKey;

	/**
	 * @var mixed
	 */
	protected $placeId;

	/**
	 * @var mixed
	 */
	protected $cardObject;

	public function __construct(FBConfig $config)
	{
		parent::__construct($config);

		$this->appId = $config->getAppId();
		$this->appShortName = $config->getAppName();
		$this->appSecretKey = $config->getAppSecret();
		$this->placeId = $config->get('placeId');
		$this->cardObject = $config->get('cardObject');

		$this->app = new Facebook(
			['app_id' => $config->getAppId(), 'app_secret' => $config->getAppSecret()]
		);
		$helper = $this->app->getCanvasHelper();
		try {
			try {
				$this->accessToken = $config->getAccessToken();
			} catch (SocialApiException $e) {
				if ($e->getCode() === SocialApiException::ERROR_NOT_FOUND_PARAM) {
					$this->accessToken = $helper->getAccessToken();
				} else {
					throw $e;
				}
			}
		} catch (\Exception $e) {
			FastLog::add(
				'error',
				[
					'message' => $e->getMessage(),
					'line'    => $e->getLine(),
					'file'    => $e->getFile(),
					'code'    => $e->getCode(),
				]
			);
		}

		if ($this->accessToken) {
			// Logged in.
			$config->setToSession('access_token', (string)$this->accessToken);
		}
	}

	/**
	 * @param string $method
	 * @param array  $params
	 * @param array  $config
	 *
	 * @return array
	 * @throws SocialApiException
	 */
	public function sendRequest($method, Array $params = [], Array $config = [])
	{
		if (!isset($config['socId'])) {
			$config['socId'] = 'me';
		}

		return $this->apiRequest('/' . $config['socId'], $method, ...$params);
	}

	/**
	 * @param        $path
	 * @param string $method
	 * @param null   $parameters
	 * @param null   $version
	 * @param null   $etag
	 *
	 * @return array
	 * @throws SocialApiException
	 */
	private function apiRequest($path,
	                            $method = "GET",
	                            $parameters = null,
	                            $version = null,
	                            $etag = null)
	{
		$response = [];
		try {
			switch ($method) {
				case 'GET':
					$request = $this->app->get($path, $this->accessToken, $etag, $version);
					$response = $request->getDecodedBody();
					break;
				case 'POST':
					$request = $this->app->post($path, $parameters, $this->accessToken, $etag, $version);
					$response = $request->getDecodedBody();
					break;
				case 'DELETE':
					$request = $this->app->delete($path, $parameters, $this->accessToken, $etag, $version);
					$response = $request->getDecodedBody();
					break;
				default:
					break;
			}
		} catch (\Exception $e) {
			FastLog::add(
				'ERROR',
				[
					'message' => $e->getMessage(),
					'code'    => $e->getCode(),
					'file'    => $e->getFile(),
					'line'    => $e->getLine(),
				]
			);
		}

		return $response;
	}

	/**
	 * @param null $socId
	 *
	 * @return mixed
	 */
	private function getRefUserId($socId = null)
	{
		$refUserId = false;

		$requestIds = (string)Request::getGetOrPost('request_ids', '');
		$actionIds = (string)Request::getGetOrPost('fb_action_ids', '');

		if (!empty($requestIds)) {
			$requestIds = explode(',', $requestIds);
		} else {
			$requestIds = [];
		}

		if (!empty($actionIds)) {
			$actionIds = explode(',', $actionIds);
		} else {
			$actionIds = [];
		}

		$objectIds = array_merge($requestIds, $actionIds);
		$objectIds = implode(',', $objectIds);

		if (!empty($objectIds) && !empty($socId)) {
			$objects = $this->apiRequest(
				'/',
				'GET',
				['ids' => $objectIds],
				null,
				null
			);
			foreach ($objects as $object) {
				if (!empty($object->from->id) && $object->from->id !== $socId) {
					$refUserId = $object->from->id;
					break;
				}
			}
		}

		return $refUserId;
	}

	/**
	 * @return array
	 */
	public function decodeExternalData()
	{
		return [];
	}

	/**
	 * @param null  $socId
	 * @param array $args
	 *
	 * @return mixed
	 */
	public function getRef($socId = null, Array $args = [])
	{
		$refId = null;
		$refPostId = null;
		$refUserId = null;
		if (isset($args['referer']) && !empty($args['referer'])) {
			$refSrc = $args['referer'];
		} else {
			$refSrc = (string)Request::getGetOrPost('fb_source', 'direct');
			$refId = (string)Request::getGetOrPost('ref', '');
			$refPostId = (string)Request::getGetOrPost('post_id', '');
			$refUserId = $this->getRefUserId($socId);
		}

		return new SocialRef($refSrc, $refId, $refUserId, $refPostId);
	}

	/**
	 * @param array $config
	 *
	 * @return mixed
	 */
	public function getFriendAppList(Array $config = [])
	{
		if (!isset($config['socId'])) {
			$config['socId'] = 'me';
		}

		if (!isset($config['perPage'])) {
			$config['perPage'] = 1000;
		}
		if (!isset($config['page'])) {
			$config['page'] = 0;
		}

		$friends = [];
		$fbFriends = $this->sendRequest(
			'/' . $config['socId'] . '/friends?fields=installed&limit=' . $config['perPage'] . '&offset=' . $config['page'] . ''
		);

		if (isset($fbFriends['data'])) {
			foreach ($fbFriends['data'] as $friend) {
				if (isset($friend->installed) && $friend->installed == true) {
					$friends[] = $friend->id;
				}
			}
		}

		return $friends;
	}

	/**
	 * @param array $config
	 *
	 * @return mixed
	 */
	public function getFriendList(Array $config = [])
	{
		if (!isset($config['socId'])) {
			$config['socId'] = 'me';
		}

		if (!isset($config['perPage'])) {
			$config['perPage'] = 1000;
		}
		if (!isset($config['page'])) {
			$config['page'] = 0;
		}

		$fbFriends = $this->sendRequest(
			'/' . $config['socId'] . '/friends?limit=' . $config['perPage'] . '&offset=' . $config['page'] . ''
		);
		$friends = isset($fbFriends['data']) ? $fbFriends['data'] : [];

		return $friends;
	}

	/**
	 * @param array $queryParams
	 * @param array $hashParams
	 *
	 * @return mixed
	 */
	public function getAppURL(Array $queryParams = [], Array $hashParams = [])
	{
		$url = $this->config->getAppURL() . $this->config->get('shortAppName', $this->appId);

		return $this->prepareURL($url, $queryParams, $hashParams);
	}

	/**
	 * @param null  $socId
	 * @param array $customFields
	 * @param array $userInfo
	 *
	 * @return mixed
	 * @throws SocialApiException
	 */
	public function getUserInfo($socId = null, Array $customFields = [], Array $userInfo = [])
	{
		//try {
		if (!isset($socId)) {
			$socId = 'me';
		}

		if (empty($customFields)) {
			$fields = implode(',', $this->userFields);
		} else {
			$fields = implode(',', $customFields);
		}

		/** @var GraphUser $user_profile */
		$graphUser = $this->app->get('/' . $socId . '?fields=' . $fields)->getGraphUser();

		/** @var $graphUser GraphUser */
		$id = $graphUser->getId();
		if (!isset($id)) {
			throw new SocialApiException('Not found required params');
		}
		$city = $graphUser->getLocation()->getField('city', '');
		$country = $graphUser->getLocation()->getField('country', '');

		/** @var GraphPicture $photoObj */
		$photoObj = $graphUser->getPicture();
		$photo = $photoObj->getUrl();

		$date = $graphUser->getBirthday();
		if ($date) {
			$birthY = $date->format('Y');
			$birthM = $date->format('m');
			$birthD = $date->format('d');
		} else {
			$birthM = 1;
			$birthD = 1;
			$birthY = 1970;
		}
		$email = $graphUser->getEmail();

		$timezone = $graphUser->getField('timezone', '');
		$first_name = $graphUser->getFirstName();
		$last_name = $graphUser->getLastName();
		$gender = $graphUser->getGender();
		$locale = $graphUser->getField('locale', '');

		$user = new SocialUser(
			FBConfig::getSocialNetwork(),
			$id,
			$first_name,
			$last_name,
			$city,
			$country,
			$gender,
			$locale,
			$birthM,
			$birthD,
			$birthY,
			$timezone,
			$photo,
			$email,
			$graphUser->asArray()
		);

		return $user;
	}

	/**
	 * @param array $array
	 *
	 * @return bool
	 * @throws SocialApiException
	 */
	public function requestValidate(Array $array)
	{
		if (isset($array['signed_request'])) {
			$signed_request = $this->parseSignedRequest($array['signed_request']);
			if (isset($signed_request['user_id'])) {
				return $signed_request['user_id'];
			}

			return false;
		} else {
			return false;
		}
	}

	/**
	 * @param array $params
	 *
	 * @return bool|string
	 * @internal param $array
	 */
	public function checkPayment(Array $params)
	{
		return $this->requestValidate($params);
	}

	/**
	 * @param $signedRequest
	 *
	 * @return bool|mixed
	 */
	private function parseSignedRequest($signedRequest)
	{
		list($encoded_sig, $payload) = explode('.', $signedRequest, 2);
		$sig = Base64URL::decode($encoded_sig);
		$data = json_decode(Base64URL::decode($payload), true);
		if (isset($data['algorithm']) == false || strtoupper($data['algorithm']) !== 'HMAC-SHA256') {
			return false;
		}
		$expectedSig = hash_hmac(
			'sha256',
			$payload,
			$this->appSecretKey,
			$raw = true
		);
		if ($sig !== $expectedSig) {
			return false;
		}

		return $data;
	}
}