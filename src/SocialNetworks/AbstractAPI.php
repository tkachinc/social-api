<?php
namespace TkachInc\SocialApi\SocialNetworks;

use TkachInc\SocialApi\SocialApiException;
use TkachInc\SocialApi\SocialConfigs\AbstractConfig;
use TkachInc\SocialApi\SocialRef;

/**
 * Class AbstractApi
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
abstract class AbstractAPI
{
	/**
	 * @var AbstractConfig
	 */
	protected $config;

	/**
	 * @var array
	 */
	protected $userFields = [];

	/**
	 * @param AbstractConfig $config
	 */
	public function __construct(AbstractConfig $config)
	{
		$this->config = $config;
	}

	/**
	 * @return AbstractConfig
	 */
	public function getConfig()
	{
		return $this->config;
	}

	/**
	 * @return string
	 * @throws SocialApiException
	 */
	public function getRedirectURL()
	{
		$url = $this->config->get('authRedirectUrl', null);
		if ($url === null) {
			throw new SocialApiException('Not found redirect url');
		}

		return $url;
	}

	/**
	 * @param       $url
	 * @param array $queryParams
	 * @param array $hashParams
	 * @return string
	 */
	protected function prepareURL($url, $queryParams = [], $hashParams = [])
	{
		if ($queryParams) {
			$url .= '?' . http_build_query($queryParams);
		}
		if ($hashParams) {
			$url .= '#' . http_build_query($hashParams);
		}

		return $url;
	}

	/**
	 * @param $fields
	 */
	public function setUserRequiredFields($fields)
	{
		$this->userFields = $fields;
	}

	/**
	 * @return array
	 */
	public function getUserRequiredFields()
	{
		return $this->userFields;
	}

	/**
	 * @param       $method
	 * @param array $params
	 * @param array $config
	 * @return mixed
	 * @throws SocialApiException
	 */
	abstract public function sendRequest($method, Array $params = [], Array $config = []);

	/**
	 * @return mixed
	 */
	abstract public function decodeExternalData();

	/**
	 * @param null $socId
	 * @param array $args
	 * @return SocialRef
	 */
	abstract public function getRef($socId = null, Array $args = []);

	/**
	 * @param array $config
	 * @return mixed
	 */
	abstract public function getFriendAppList(Array $config = []);

	/**
	 * @param array $config
	 * @return mixed
	 */
	abstract public function getFriendList(Array $config = []);

	/**
	 * @param array $queryParams
	 * @param array $hashParams
	 * @return mixed
	 */
	abstract public function getAppURL(Array $queryParams = [], Array $hashParams = []);

	/**
	 * @param null $socId
	 * @param array $customFields
	 * @param array $userInfo
	 * @return mixed
	 */
	abstract public function getUserInfo($socId = null, Array $customFields = [], Array $userInfo = []);

	/**
	 * @param array $params
	 * @return mixed
	 */
	abstract public function requestValidate(Array $params);

	/**
	 * @param array $params
	 * @return bool
	 */
	abstract public function checkPayment(Array $params);
}