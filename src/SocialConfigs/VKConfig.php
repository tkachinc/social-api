<?php
namespace TkachInc\SocialApi\SocialConfigs;

use TkachInc\SocialApi\OAuth\Classes\VKAuth;
use TkachInc\SocialApi\SocialNetworks\VKAPI;

/**
 * Created by PhpStorm.
 * User: maxim.tkach
 * Date: 12/30/16
 * Time: 01:27
 */
class VKConfig extends AbstractConfig
{

    /**
     * @return string
     */
    public static function getSocialNetwork()
    {
        return 'vk';
    }

    /**
     * @return string
     */
    public function getAppURL()
    {
        return 'https://vk.com/app/';
    }

    /**
     * @return string
     */
    public function getClassAuth()
    {
        return VKAuth::class;
    }

    /**
     * @return string
     */
    public function getAPIURL()
    {
        return 'https://api.vk.com/method/';
    }

    /**
     * @return string
     */
    public function getClassAPI()
    {
        return VKAPI::class;
    }

    /**
     * @var string|null
     */
    protected $accessToken;

    /**
     * @var string|null
     */
    protected $authKey;

    /**
     * @return mixed
     */
    public function getAppId()
    {
        return $this->getConfig()->get([static::getSocialNetwork(), 'appId']);
    }

    /**
     * @return mixed
     */
    public function getAppSecret()
    {
        return $this->getConfig()->get([static::getSocialNetwork(), 'appSecret']);
    }

    /**
     * @param null $accessToken
     *
     * @return mixed
     */
    public function getAccessToken($accessToken = null)
    {
        return $this->getExternalSocialKey('accessToken', 'access_token', $accessToken);
    }

    /**
     * @param null $authKey
     *
     * @return mixed
     */
    public function getAuthKey($authKey = null)
    {
        return $this->getExternalSocialKey('authKey', 'auth_sig', $authKey);
    }
}