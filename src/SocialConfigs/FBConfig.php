<?php
namespace TkachInc\SocialApi\SocialConfigs;

use TkachInc\SocialApi\OAuth\Classes\FBAuth;
use TkachInc\SocialApi\SocialNetworks\FBAPI;

/**
 * Created by PhpStorm.
 * User: maxim.tkach
 * Date: 12/30/16
 * Time: 01:27
 */
class FBConfig extends AbstractConfig
{

    /**
     * @return string
     */
    public static function getSocialNetwork()
    {
        return 'fb';
    }

    /**
     * @return string
     */
    public function getAppURL()
    {
        return 'https://apps.facebook.com/';
    }

    /**
     * @return string
     */
    public function getAPIURL()
    {
        return 'https://graph.facebook.com/';
    }

    /**
     * @return string
     */
    public function getClassAPI()
    {
        return FBAPI::class;
    }

    /**
     * @return string
     */
    public function getClassAuth()
    {
        return FBAuth::class;
    }

    /**
     * @var string|null
     */
    protected $accessToken;

    /**
     * @var string|null
     */
    protected $authKey;

    /**
     * @var string|null
     */
    protected $signedRequest;

    /**
     * @return mixed
     */
    public function getAppId()
    {
        return $this->getConfig()->get([static::getSocialNetwork(), 'appId']);
    }

    /**
     * @return mixed
     */
    public function getAppSecret()
    {
        return $this->getConfig()->get([static::getSocialNetwork(), 'appSecret']);
    }

    /**
     * @return mixed
     */
    public function getAppName()
    {
        return $this->getConfig()->get([static::getSocialNetwork(), 'appName']);
    }

    /**
     * @param null $accessToken
     *
     * @return mixed
     */
    public function getAccessToken($accessToken = null)
    {
        return $this->getExternalSocialKey('accessToken', 'access_token', $accessToken);
    }

    /**
     * @param null $authKey
     *
     * @return mixed
     */
    public function getAuthKey($authKey = null)
    {
        return $this->getExternalSocialKey('authKey', 'auth_key', $authKey);
    }

    /**
     * @param null $signedRequest
     *
     * @return mixed
     */
    public function getSignedRequest($signedRequest = null)
    {
        return $this->getExternalSocialKey('signedRequest', 'signed_request', $signedRequest);
    }
}