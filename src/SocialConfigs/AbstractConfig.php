<?php
/**
 * Created by PhpStorm.
 * User: maxim.tkach
 * Date: 12/30/16
 * Time: 01:35
 */

namespace TkachInc\SocialApi\SocialConfigs;

use Evenement\EventEmitter;
use TkachInc\Core\Config\Config;
use TkachInc\Core\Storage\Wrappers\Session;
use TkachInc\Core\URLFacade;
use TkachInc\Engine\Services\Request\Request;
use TkachInc\SocialApi\SocialApiException;
use TkachInc\SocialApi\SocialNetworks\AbstractAPI;

abstract class AbstractConfig extends EventEmitter
{
	/**
	 * @var string
	 */
	protected $configInstance;

	/**
	 * @var bool
	 */
	protected $isTestHost;

	/**
	 * @var Session
	 */
	protected $session;

	/**
	 * AbstractConfig constructor.
	 *
	 * @param bool   $isTestHost
	 * @param string $configInstance
	 */
	public function __construct($isTestHost = false, $configInstance = 'main')
	{
		$this->session = new Session();

		$this->isTestHost = $isTestHost;
		$this->configInstance = $configInstance;
	}

	/**
	 * @return mixed
	 */
	public function getBaseURL()
	{
		return URLFacade::getBaseURL();
	}

	/**
	 * @return bool
	 */
	public function isTestHost()
	{
		return $this->isTestHost;
	}

	/**
	 * @return mixed
	 */
	abstract public static function getSocialNetwork();

	/**
	 * @return mixed
	 */
	abstract public function getAppURL();

	/**
	 * @return mixed
	 */
	abstract public function getAPIURL();

	/**
	 * @return mixed
	 */
	abstract public function getClassAPI();

	/**
	 * @return mixed
	 */
	abstract public function getClassAuth();

	/**
	 * @param $key
	 *
	 * @return string
	 */
	protected function getSessionKey($key)
	{
		return 'social.' . static::getSocialNetwork() . '.' . $key;
	}

	/**
	 * @param $key
	 * @param $value
	 *
	 * @return $this
	 */
	public function setToSession($key, $value)
	{
		$this->session->set(static::getSessionKey($key), $value);

		return $this;
	}

	/**
	 * @param      $key
	 * @param null $default
	 *
	 * @return null
	 */
	public function getFromSession($key, $default = null)
	{
		return $this->session->get(static::getSessionKey($key), $default);
	}

	/**
	 * @param $name
	 * @param $key
	 * @param $default
	 *
	 * @return mixed
	 * @throws SocialApiException
	 */
	protected function getExternalSocialKey($name, $key, $default)
	{
		if (!$this->{$name}) {
			$default = $default??Request::getRequest($key, $this->getFromSession($key));
			if (!$default) {
				throw new SocialApiException('Not found ' . $key, SocialApiException::ERROR_NOT_FOUND_PARAM);
			}
			$this->setToSession($key, $default);
			$this->{$name} = $default;
		}

		return $this->{$name};
	}

	/**
	 * @return Config
	 */
	protected function getConfig()
	{
		return Config::getInstance($this->configInstance);
	}

	/**
	 * @return AbstractAPI
	 * @throws SocialApiException
	 */
	public function getAPI()
	{
		$class = $this->getClassAPI();
		if (!class_exists($class)) {
			throw new SocialApiException('Class not found: ' . $class, SocialApiException::ERROR_NOT_FOUND_CLASS);
		}

		$api = new $class($this);

		return $api;
	}

	/**
	 * @param      $key
	 * @param null $default
	 *
	 * @return mixed
	 */
	public function get($key, $default = null)
	{
		return $this->getConfig()->get([static::getSocialNetwork(), $key], $default);
	}

	/**
	 * @return mixed
	 * @throws SocialApiException
	 */
	public function getAuth()
	{
		$class = $this->getClassAuth();
		if (!class_exists($class)) {
			throw new SocialApiException('Class not found: ' . $class, SocialApiException::ERROR_NOT_FOUND_CLASS);
		}

		$api = new $class($this);

		return $api;
	}
}