<?php
namespace TkachInc\SocialApi\SocialConfigs;

use TkachInc\SocialApi\OAuth\Classes\OKAuth;
use TkachInc\SocialApi\SocialNetworks\OKAPI;

/**
 * Created by PhpStorm.
 * User: maxim.tkach
 * Date: 12/30/16
 * Time: 01:27
 */
class OKConfig extends AbstractConfig
{

    /**
     * @return string
     */
    public static function getSocialNetwork()
    {
        return 'ok';
    }

    /**
     * @return string
     */
    public function getAppURL()
    {
        return 'https://www.odnoklassniki.ru/game/';
    }

    /**
     * @return string
     */
    public function getAPIURL()
    {
        return 'https://api.odnoklassniki.ru/fb.do';
    }

    /**
     * @return string
     */
    public function getClassAPI()
    {
        return OKAPI::class;
    }

    /**
     * @return string
     */
    public function getClassAuth()
    {
        return OKAuth::class;
    }

    /**
     * @var string|null
     */
    protected $sessionKey;

    /**
     * @var string|null
     */
    protected $sessionSecretKey;

    /**
     * @return mixed
     */
    public function getAppId()
    {
        return $this->getConfig()->get([static::getSocialNetwork(), 'appId']);
    }

    /**
     * @return mixed
     */
    public function getAppSecret()
    {
        return $this->getConfig()->get([static::getSocialNetwork(), 'appSecret']);
    }

    /**
     * @return mixed
     */
    public function getAppName()
    {
        return $this->getConfig()->get([static::getSocialNetwork(), 'appName']);
    }

    /**
     * @param null $sessionKey
     *
     * @return mixed
     */
    public function getSessionKey($sessionKey = null)
    {
        return $this->getExternalSocialKey('sessionKey', 'session_key', $sessionKey);
    }

    /**
     * @param null $sessionKey
     *
     * @return mixed
     */
    public function getSessionSecretKey($sessionKey = null)
    {
        return $this->getExternalSocialKey('sessionSecretKey', 'session_secret_key', $sessionKey);
    }
}