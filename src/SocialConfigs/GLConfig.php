<?php
namespace TkachInc\SocialApi\SocialConfigs;
use TkachInc\SocialApi\OAuth\Classes\GLAuth;
use TkachInc\SocialApi\SocialApiException;

/**
 * Created by PhpStorm.
 * User: maxim.tkach
 * Date: 12/30/16
 * Time: 01:27
 */
class GLConfig extends AbstractConfig
{
	/**
	 * @return string
	 */
	public static function getSocialNetwork()
	{
		return 'gl';
	}

	/**
	 * @return string
	 */
	public function getAppURL()
	{
		return '';
	}

	/**
	 * @return string
	 */
	public function getAPIURL()
	{
		return '';
	}

	/**
	 * @return string
	 * @throws SocialApiException
	 */
	public function getClassAPI()
	{
		throw new SocialApiException('Not found API');
	}
	/**
	 * @return string
	 */
	public function getClassAuth()
	{
		return GLAuth::class;
	}

	/**
	 * @var string|null
	 */
	protected $accessToken;

	/**
	 * @var string|null
	 */
	protected $authKey;

	/**
	 * @var string|null
	 */
	protected $signedRequest;

	/**
	 * @return mixed
	 */
	public function getAppId()
	{
		return $this->getConfig()->get([static::getSocialNetwork(), 'appId']);
	}

	/**
	 * @return mixed
	 */
	public function getAppSecret()
	{
		return $this->getConfig()->get([static::getSocialNetwork(), 'appSecret']);
	}
}