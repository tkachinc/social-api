<?php
namespace TkachInc\SocialApi;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class SocialUser
{
	protected $sn;
	protected $socId;
	protected $email;
	protected $firstName;
	protected $lastName;
	protected $city;
	protected $country;
	protected $gender;
	protected $locale;
	protected $birthTimestamp = null;
	protected $birthM;
	protected $birthD;
	protected $birthY;
	protected $age = null;
	protected $timezone;
	protected $photo;
	protected $params;

	/**
	 * @param        $sn
	 * @param        $socId
	 * @param        $firstName
	 * @param        $lastName
	 * @param        $city
	 * @param        $country
	 * @param        $gender
	 * @param        $locale
	 * @param        $birthM
	 * @param        $birthD
	 * @param        $birthY
	 * @param        $timezone
	 * @param        $photo
	 * @param string $email
	 * @param array $params
	 */
	public function __construct($sn,
	                            $socId,
	                            $firstName,
	                            $lastName,
	                            $city,
	                            $country,
	                            $gender,
	                            $locale,
	                            $birthM,
	                            $birthD,
	                            $birthY,
	                            $timezone,
	                            $photo,
	                            $email = '',
	                            Array $params = [])
	{
		$this->sn = $sn;
		$this->socId = $socId;
		$this->email = $email;
		$this->firstName = $firstName;
		$this->lastName = $lastName;
		$this->city = $city;
		$this->country = $country;
		$this->locale = $locale;
		$this->birthM = $birthM;
		$this->birthD = $birthD;
		$this->birthY = $birthY;
		$this->timezone = $timezone;
		$this->photo = $photo;
		$this->gender = $gender;
		$this->params = $params;

		if ($this->birthM !== 0 && $this->birthD !== 0 && $this->birthY !== 0) {
			$birthday = new \DateTime();
			$birthday->setDate($this->birthY, $this->birthM, $this->birthD);

			$current = new \DateTime();
			$interval = $current->diff($birthday);
			$this->age = $interval->format('%Y%');
			$this->birthTimestamp = $birthday->getTimestamp();
		}
	}

	/**
	 * @return string
	 */
	public function getAge()
	{
		return $this->age;
	}

	/**
	 * @return int
	 */
	public function getBirthTimestamp()
	{
		return $this->birthTimestamp;
	}

	/**
	 * @param            $key
	 * @param bool|false $default
	 * @return bool
	 */
	public function getParam($key, $default = false)
	{
		return isset($this->params[$key]) ? $this->params[$key] : $default;
	}

	public function getSn()
	{
		return $this->sn;
	}

	public function getSocId()
	{
		return $this->socId;
	}

	/**
	 * @return string
	 */
	public function getEmail()
	{
		return $this->email;
	}

	public function getFirstName()
	{
		return $this->firstName;
	}

	public function getLastName()
	{
		return $this->lastName;
	}

	public function getCity()
	{
		return $this->city;
	}

	public function getCountry()
	{
		return $this->country;
	}

	public function getGender()
	{
		return $this->gender;
	}

	public function getLocale()
	{
		return $this->locale;
	}

	public function getBirthM()
	{
		return $this->birthM;
	}

	public function getBirthD()
	{
		return $this->birthD;
	}

	public function getBirthY()
	{
		return $this->birthY;
	}

	public function getTimezone()
	{
		return $this->timezone;
	}

	public function getPhoto()
	{
		return $this->photo;
	}
}