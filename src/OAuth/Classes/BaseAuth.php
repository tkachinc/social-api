<?php
namespace TkachInc\SocialApi\OAuth\Classes;

use TkachInc\SocialApi\SocialConfigs\AbstractConfig;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
abstract class BaseAuth implements AuthInterface
{
	const EVENT_GET_ACCESS_TOKEN = 'event.get.access.token';

	/**
	 * @var AbstractConfig
	 */
	protected $config;

	/**
	 * @param AbstractConfig $config
	 */
	public function __construct(AbstractConfig $config)
	{
		$this->config = $config;
	}

	/**
	 * @return mixed
	 */
	abstract public function getUrlLogin();

	/**
	 * @return mixed
	 */
	abstract public function auth();

	/**
	 * @return string
	 */
	public function getRedirectUrl()
	{
		return $this->config->get('redirectUrl');
	}
} 