<?php
namespace TkachInc\SocialApi\OAuth\Classes;

use TkachInc\Engine\Services\Request\CURL\CURLManager;
use TkachInc\SocialApi\SocialApiException;
use TkachInc\SocialApi\SocialConfigs\FBConfig;
use TkachInc\SocialApi\SocialUser;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class FBAuth extends BaseAuth implements AuthInterface
{
	protected static $userFields = [
		'id',
		'first_name',
		'last_name',
		'gender',
		'email',
		'locale',
		'location',
		'birthday',
		'timezone',
		'picture.width(200).height(200)',
	];

	/**
	 * @var FBConfig
	 */
	protected $config;

	/**
	 * @param FBConfig $config
	 */
	public function __construct(FBConfig $config)
	{
		parent::__construct($config);
	}

	/**
	 * @return string
	 */
	public function getUrlLogin()
	{
		$permissionsRequired = [
			'publish_actions',
			'email',
			'user_friends',
			//			'user_birthday',
			//			'user_likes',
			//			'user_location'
		];

		$link = 'https://www.facebook.com/dialog/oauth?' . http_build_query(
				[
					'client_id'     => $this->config->getAppId(),
					'redirect_uri'  => $this->getRedirectUrl(),
					'response_type' => 'code',
					'scope'         => implode(',', $permissionsRequired),
				],
				'',
				'&'
			);

		return $link;
	}

	/**
	 * @return mixed
	 * @throws SocialApiException
	 */
	public function auth()
	{
		if (isset($_REQUEST['code'])) {
			$curl = new CURLManager('https://graph.facebook.com/oauth/access_token');
			$curl->sendGet(
				[
					'client_id'     => $this->config->getAppId(),
					'redirect_uri'  => $this->getRedirectUrl(),
					'client_secret' => $this->config->getAppSecret(),
					'code'          => $_REQUEST['code'],
				]
			)->exec();
			$response = $curl->getContent();
			parse_str($response, $paramsResponse);

			$config = $this->config;
			if (isset($paramsResponse['access_token'])) {
				$this->config->emit(
					BaseAuth::EVENT_GET_ACCESS_TOKEN,
					[$paramsResponse['access_token'], $paramsResponse, $config::getSocialNetwork()]
				);

				$request = implode(',', static::$userFields);
				$curl = new CURLManager('https://graph.facebook.com/me');
				$curl->sendGet(['access_token' => $paramsResponse['access_token'], 'fields' => $request]);
				$curl->setOption(CURLOPT_RETURNTRANSFER, 1)->setOption(
					CURLOPT_FOLLOWLOCATION,
					1
				);
				$curl->exec();
				$response = $curl->getContent();
				$userInfo = json_decode($response, true);

				if (!isset($userInfo['id'])) {
					throw new SocialApiException('Not found required params');
				}

				if (isset($userInfo['location']['name'])) {
					$location = explode(',', $userInfo['location']['name']);
				}

				$city = isset($location[0]) ? trim($location[0]) : '';
				$country = isset($location[1]) ? trim($location[1]) : '';
				$photo = isset($userInfo['picture']['data']['url']) ? $userInfo['picture']['data']['url'] : '';

				$dataArray = [];
				if (isset($userInfo['birthday'])) {
					$dataArray = explode('/', $userInfo['birthday']);
				}
				$birthM = (string)isset($dataArray[0]) ? $dataArray[0] : '';
				$birthD = (string)isset($dataArray[1]) ? $dataArray[1] : '';
				$birthY = (string)isset($dataArray[2]) ? $dataArray[2] : '';

				$user = new SocialUser(
					$config::getSocialNetwork(),
					$userInfo['id'],
					isset($userInfo['first_name']) ? $userInfo['first_name'] : '',
					isset($userInfo['last_name']) ? $userInfo['last_name'] : '',
					$city,
					$country,
					isset($userInfo['gender']) ? $userInfo['gender'] : '',
					isset($userInfo['locale']) ? $userInfo['locale'] : '',
					$birthM,
					$birthD,
					$birthY,
					isset($userInfo['timezone']) ? $userInfo['timezone'] : '',
					$photo,
					isset($userInfo['email']) ? $userInfo['email'] : ''
				);

				return $user;
			}
		}

		throw new SocialApiException('Login error');
	}
} 