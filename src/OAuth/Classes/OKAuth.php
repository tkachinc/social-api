<?php
namespace TkachInc\SocialApi\OAuth\Classes;

use TkachInc\Engine\Services\Request\CURL\CURLManager;
use TkachInc\SocialApi\SocialApiException;
use TkachInc\SocialApi\SocialConfigs\OKConfig;
use TkachInc\SocialApi\SocialUser;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class OKAuth extends BaseAuth implements AuthInterface
{
	/**
	 * @var array
	 */
	protected static $userFields = [
		'uid',
		'first_name',
		'last_name',
		'gender',
		'locale',
		'location',
		'birthday',
		'pic_3',
	];

	/**
	 * @var OKConfig
	 */
	protected $config;

	/**
	 * @param OKConfig $config
	 */
	public function __construct(OKConfig $config)
	{
		parent::__construct($config);
	}

	/**
	 * @return string
	 */
	public function getUrlLogin()
	{
		$permissionsRequired = [
			'VALUABLE_ACCESS',
		];

		$link = 'http://www.odnoklassniki.ru/oauth/authorize?' . http_build_query(
				[
					'client_id'     => $this->config->getAppId(),
					'redirect_uri'  => $this->getRedirectUrl(),
					'response_type' => 'code',
					'scope'         => implode(';', $permissionsRequired),
				],
				'',
				'&'
			);

		return $link;
	}

	/**
	 * @return mixed
	 * @throws SocialApiException
	 */
	public function auth()
	{
		if (isset($_REQUEST['code'])) {
			$curl = new CURLManager('http://api.odnoklassniki.ru/oauth/token.do');
			$curl->sendPost(
				[
					'client_id'     => $this->config->getAppId(),
					'redirect_uri'  => $this->getRedirectUrl(),
					'client_secret' => $this->config->getAppSecret(),
					'code'          => $_REQUEST['code'],
					'grant_type'    => 'authorization_code',
				]
			)->exec();
			$paramsResponse = json_decode($curl->getContent(), true);

			$config = $this->config;
			if (isset($paramsResponse['access_token'])) {
				$this->config->emit(
					BaseAuth::EVENT_GET_ACCESS_TOKEN,
					[$paramsResponse['access_token'], $paramsResponse, $config::getSocialNetwork()]
				);

				$fields = implode(',', static::$userFields);
				$sign = md5(
					'application_key=' . $this->config->getAppId() . 'method=users.getCurrentUser' . md5(
						$paramsResponse['access_token'] . $this->config->getAppSecret()
					)
				);
				$curl = new CURLManager('http://api.odnoklassniki.ru/fb.do');
				$curl->sendGet(
					[
						'method'          => 'users.getCurrentUser',
						'application_key' => $this->config->getAppId(),
						'fields'          => $fields,
						'access_token'    => $paramsResponse['access_token'],
						'sig'             => $sign,
					]
				)->exec();
				$response = $curl->getContent();
				$userInfo = json_decode($response, true);

				if (empty($userInfo) || !isset($userInfo['uid'])) {
					throw new SocialApiException('Not found required params');
				}

				$city = $userInfo['location']['city'];
				$country = $userInfo['location']['country'];

				$dataArray = [];
				if (isset($userInfo['birthday'])) {
					$dataArray = explode('-', $userInfo['birthday']);
				}
				$birthD = (string)isset($dataArray[2]) ? $dataArray[2] : '';
				$birthM = (string)isset($dataArray[1]) ? $dataArray[1] : '';
				$birthY = (string)isset($dataArray[0]) ? $dataArray[0] : '';

				$user = new SocialUser(
					$config::getSocialNetwork(),
					$userInfo['uid'],
					isset($userInfo['first_name']) ? $userInfo['first_name'] : '',
					isset($userInfo['last_name']) ? $userInfo['last_name'] : '',
					$city,
					$country,
					isset($userInfo['gender']) ? $userInfo['gender'] : '',
					isset($userInfo['locale']) ? $userInfo['locale'] : '',
					$birthM,
					$birthD,
					$birthY,
					isset($userInfo['timezone']) ? $userInfo['timezone'] : '',
					isset($userInfo['pic_big']) ? $userInfo['pic_big'] : '',
					isset($userInfo['email']) ? $userInfo['email'] : ''
				);

				return $user;
			}
		}

		throw new SocialApiException('Login error');
	}
} 