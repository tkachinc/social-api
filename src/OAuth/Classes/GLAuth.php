<?php
namespace TkachInc\SocialApi\OAuth\Classes;

use TkachInc\Engine\Services\Request\CURL\CURLManager;
use TkachInc\SocialApi\SocialApiException;
use TkachInc\SocialApi\SocialConfigs\GLConfig;
use TkachInc\SocialApi\SocialUser;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class GLAuth extends BaseAuth implements AuthInterface
{
	/**
	 * @var GLConfig
	 */
	protected $config;

	/**
	 * @param GLConfig $config
	 */
	public function __construct(GLConfig $config)
	{
		parent::__construct($config);
	}

	/**
	 * @return string
	 */
	public function getUrlLogin()
	{
		$link = 'https://accounts.google.com/o/oauth2/auth?' .
			http_build_query(
				[
					'client_id'     => $this->config->getAppId(),
					'redirect_uri'  => $this->getRedirectUrl(),
					'response_type' => 'code',
					//				'scope' => 'https://www.googleapis.com/auth/userinfo.email+https://www.googleapis.com/auth/userinfo.profile'
				],
				'',
				'&'
			) .
			'&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile';

		return $link;
	}

	/**
	 * @return mixed
	 * @throws SocialApiException
	 */
	public function auth()
	{
		if (isset($_REQUEST['code'])) {
			$curl = new CURLManager('https://accounts.google.com/o/oauth2/token');
			$curl->sendPost(
				[
					'code'          => $_REQUEST['code'],
					'client_id'     => $this->config->getAppId(),
					'client_secret' => $this->config->getAppSecret(),
					'redirect_uri'  => $this->getRedirectUrl(),
					'grant_type'    => 'authorization_code',
				]
			)->exec();
			$paramsResponse = json_decode($curl->getContent(), true);

			$config = $this->config;
			if (isset($paramsResponse['access_token'])) {
				$this->config->emit(
					BaseAuth::EVENT_GET_ACCESS_TOKEN,
					[$paramsResponse['access_token'], $paramsResponse, $config::getSocialNetwork()]
				);

				$curl = new CURLManager('https://www.googleapis.com/oauth2/v1/userinfo');
				$curl->sendGet(
					['access_token' => $paramsResponse['access_token']]
				);
				$curl->setOption(CURLOPT_RETURNTRANSFER, 1)->setOption(
					CURLOPT_FOLLOWLOCATION,
					1
				);
				$curl->exec();
				$response = $curl->getContent();
				$userInfo = json_decode($response, true);

				$user = new SocialUser(
					$config::getSocialNetwork(),
					$userInfo['id'],
					isset($userInfo['given_name']) ? $userInfo['given_name'] : '',
					isset($userInfo['family_name']) ? $userInfo['family_name'] : '',
					'',
					'',
					isset($userInfo['gender']) ? $userInfo['gender'] : '',
					isset($userInfo['locale']) ? $userInfo['locale'] : '',
					'',
					'',
					'',
					isset($userInfo['timezone']) ? $userInfo['timezone'] : '',
					isset($userInfo['picture']) ? $userInfo['picture'] : '',
					isset($userInfo['email']) ? $userInfo['email'] : ''
				);

				return $user;
			}
		}

		throw new SocialApiException('Login error');
	}
} 