<?php
namespace TkachInc\SocialApi\OAuth\Classes;

use TkachInc\Engine\Services\Request\CURL\CURLManager;
use TkachInc\SocialApi\SocialApiException;
use TkachInc\SocialApi\SocialConfigs\VKConfig;
use TkachInc\SocialApi\SocialUser;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class VKAuth extends BaseAuth implements AuthInterface
{
	protected static $userFields = [
		'uid',
		'first_name',
		'last_name',
		'sex',
		'email',
		'locale',
		'country',
		'city',
		'location',
		'bdate',
		'timezone',
		'photo_200',
	];

	/**
	 * @var VKConfig
	 */
	protected $config;

	/**
	 * @param VKConfig $config
	 */
	public function __construct(VKConfig $config)
	{
		parent::__construct($config);
	}

	/**
	 * @return string
	 */
	public function getUrlLogin()
	{
		$permissionsRequired = [
			'friends',
			'notify',
		];

		$link = 'https://oauth.vk.com/authorize?' . http_build_query(
				[
					'client_id'     => $this->config->getAppId(),
					'redirect_uri'  => $this->getRedirectUrl(),
					'response_type' => 'code',
					'scope'         => implode(',', $permissionsRequired),
				],
				'',
				'&'
			);

		return $link;
	}

	/**
	 * @return mixed
	 * @throws SocialApiException
	 */
	public function auth()
	{
		if (isset($_REQUEST['code'])) {
			$curl = new CURLManager('https://oauth.vk.com/access_token');
			$curl->sendGet(
				[
					'client_id'     => $this->config->getAppId(),
					'redirect_uri'  => $this->getRedirectUrl(),
					'client_secret' => $this->config->getAppSecret(),
					'code'          => $_REQUEST['code'],
				]
			)->exec();
			$paramsResponse = json_decode($curl->getContent(), true);

			$config = $this->config;
			if (isset($paramsResponse['access_token']) && isset($paramsResponse['user_id'])) {
				$this->config->emit(
					BaseAuth::EVENT_GET_ACCESS_TOKEN,
					[$paramsResponse['access_token'], $paramsResponse, $config::getSocialNetwork()]
				);

				$fields = implode(',', static::$userFields);

				$curl = new CURLManager('https://api.vk.com/method/users.get');
				$curl->sendGet(
					[
						'uids'         => $paramsResponse['user_id'],
						'access_token' => $paramsResponse['access_token'],
						'fields'       => $fields,
					]
				);
				$curl->setOption(CURLOPT_RETURNTRANSFER, 1)->setOption(
					CURLOPT_FOLLOWLOCATION,
					1
				);
				$curl->exec();
				$response = $curl->getContent();
				$userInfo = json_decode($response, true);

				$userInfo = isset($userInfo['response'][0]) ? $userInfo['response'][0] : [];
				if (empty($userInfo) || !isset($userInfo['uid'])) {
					throw new SocialApiException('Not found required params');
				}

				$city = isset($userInfo['city']['title']) ? trim($userInfo['city']['title']) : '';
				$country = isset($userInfo['country']['title']) ? trim($userInfo['country']['title']) : '';

				$dataArray = [];
				if (isset($userInfo['bdate'])) {
					$dataArray = explode('-', $userInfo['birthday']);
				}
				$birthD = (string)isset($dataArray[0]) ? $dataArray[0] : '';
				$birthM = (string)isset($dataArray[1]) ? $dataArray[1] : '';
				$birthY = (string)isset($dataArray[2]) ? $dataArray[2] : '';

				$gender = ($userInfo['sex'] == 2) ? 'male' : 'female';

				$user = new SocialUser(
					$config::getSocialNetwork(),
					$userInfo['uid'],
					isset($userInfo['first_name']) ? $userInfo['first_name'] : '',
					isset($userInfo['last_name']) ? $userInfo['last_name'] : '',
					$city,
					$country,
					$gender,
					isset($userInfo['locale']) ? $userInfo['locale'] : '',
					$birthM,
					$birthD,
					$birthY,
					isset($userInfo['timezone']) ? $userInfo['timezone'] : '',
					isset($userInfo['photo_200']) ? $userInfo['photo_200'] : '',
					isset($userInfo['email']) ? $userInfo['email'] : ''
				);

				return $user;
			}
		}

		throw new SocialApiException('Login error');
	}
} 