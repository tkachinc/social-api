<?php
namespace TkachInc\SocialApi\OAuth\Classes;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
interface AuthInterface
{
	/**
	 * Получить валидную ссылку
	 *
	 * @return mixed
	 */
	public function getUrlLogin();

	/**
	 * Провести авторизацию на основе полученых данных
	 *
	 * @return mixed
	 */
	public function auth();
} 