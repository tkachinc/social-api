<?php
namespace TkachInc\SocialApi\OAuth\Controller;

use TkachInc\Core\Log\FastLog;
use TkachInc\Engine\Application\BaseApplication;
use TkachInc\Engine\Application\BaseController;
use TkachInc\Engine\Services\Request\Request;
use TkachInc\SocialApi\SocialApiFactory;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
abstract class BaseLogin extends BaseController
{
	const EVENT_AUTH_READY = 'event.auth.ready';
	const EVENT_AUTH_REDIRECT = 'event.auth.redirect';

	protected $config;

	public function __construct(BaseApplication $application = null)
	{
		parent::__construct($application);
		$this->config = SocialApiFactory::get(Request::getRequest('sn', ''));
	}

	public function auth()
	{
		$config = $this->config;
		try {
			$user = $config->getAuth()->auth();
			$config->emit(self::EVENT_AUTH_READY, [$user, $config::getSocialNetwork()]);
		} catch (\Exception $e) {
			FastLog::add(
				'error',
				[
					'message' => $e->getMessage(),
					'file'    => $e->getFile(),
					'line'    => $e->getLine(),
					'code'    => $e->getCode(),
				]
			);
		}
		$config->emit(self::EVENT_AUTH_REDIRECT, [$config::getSocialNetwork()]);
	}
}