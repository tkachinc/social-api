<?php
namespace TkachInc\SocialApi;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class SocialRef
{
	protected $ref;
	protected $refSrc;
	protected $refUserId;
	protected $refPostId;

	/**
	 * @param string $refSrc
	 * @param string|null $refId
	 * @param string|null $refUserId
	 * @param string|null $refPostId
	 */
	public function __construct($refSrc, $refId = null, $refUserId = null, $refPostId = null)
	{
		$this->refSrc = $refSrc;
		$this->refId = $refId;
		$this->refUserId = $refUserId;
		$this->refPostId = $refPostId;
	}

	/**
	 * @return string
	 */
	public function getRef()
	{
		return $this->refId;
	}

	/**
	 * @return string
	 */
	public function getRefSrc()
	{
		return $this->refSrc;
	}

	/**
	 * @return null|string
	 */
	public function getRefUserId()
	{
		return $this->refUserId;
	}

	/**
	 * @return null|string
	 */
	public function getRefPostId()
	{
		return $this->refPostId;
	}
}