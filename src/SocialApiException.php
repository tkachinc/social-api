<?php
namespace TkachInc\SocialApi;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class SocialApiException extends \Exception
{
	const ERROR_NOT_FOUND_SN = 1;
	const ERROR_NOT_FOUND_PARAM = 2;
	const ERROR_SOCIAL_API = 3;
	const ERROR_NOT_FOUND_CLASS = 4;
}