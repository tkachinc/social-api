<?php
namespace TkachInc\SocialApi;

use TkachInc\SocialApi\SocialConfigs\AbstractConfig;
use TkachInc\SocialApi\SocialConfigs\FBConfig;
use TkachInc\SocialApi\SocialConfigs\OKConfig;
use TkachInc\SocialApi\SocialConfigs\VKConfig;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class SocialApiFactory
{
	/**
	 * @param string $sn
	 * @param bool   $isTestHost
	 * @param string $configInstance
	 *
	 * @return AbstractConfig
	 * @throws SocialApiException
	 */
	public static function get($sn, $isTestHost = false, $configInstance = 'main')
	{
		switch ($sn) {
			case OKConfig::getSocialNetwork():
				return new OKConfig($isTestHost, $configInstance);
				break;
			case FBConfig::getSocialNetwork():
				return new FBConfig($isTestHost, $configInstance);
				break;
			case VKConfig::getSocialNetwork():
				return new VKConfig($isTestHost, $configInstance);
				break;
			default:
				throw new SocialApiException(
					'Not found platform: ' . $sn, SocialApiException::ERROR_NOT_FOUND_SN
				);
				break;
		}
	}
}